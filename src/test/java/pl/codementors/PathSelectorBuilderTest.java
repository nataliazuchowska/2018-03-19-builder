package pl.codementors;

import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.*;

@RunWith(MockitoJUnitRunner.class)
public class PathSelectorBuilderTest {

    private PathSelectorBuilder pathSelectorBuilder;

    @Before
    public void setUp() throws Exception {
        pathSelectorBuilder = new PathSelectorBuilder();
    }


    @Test
    public void withFormGroupParameter (){
        PathSelectorBuilder pathBuilder = new PathSelectorBuilder();
        pathBuilder.withFormGroup("form_group");
        pathBuilder.checkParameterFormControl();
        String form_group = "form_group";

        Assertions.assertThat(pathBuilder.withFormGroup("form_group")).isNotNull();
        Assertions.assertThat(pathBuilder
                .withFormGroup("form_group")
                .build())
                .isEqualTo("/div[@class=’" + form_group + "’]/");
    }

    @Test
    public void withFormGroupAndSpanParameters(){
        PathSelectorBuilder pathBuilder = new PathSelectorBuilder();
        pathBuilder.withFormGroup("form_group");
        pathBuilder.withSpan("span");
        pathBuilder.checkParameterFormControl();
        pathBuilder.checkParameterSpan();

        Assertions.assertThat(pathBuilder.withFormGroup("form_group").withSpan("span")).isNotNull();
        Assertions.assertThat(pathBuilder.
                withFormGroup("form_group").
                withSpan("span").
                build()).
                isEqualTo("/div[@class=’form_group’]/span[@class=’’]");
    }

    @Test
    public void withFormGroupSpanAndUserMessageParameters(){
        PathSelectorBuilder pathBuilder = new PathSelectorBuilder();
        pathBuilder.withFormGroup("form_group");
        pathBuilder.withSpan("span");
        pathBuilder.withUserMessage("");

        pathBuilder.checkParameterFormGroup();
        pathBuilder.checkParameterSpan();
        pathBuilder.checkParameterUserMessage();

        Assertions.assertThat(pathBuilder
                .withFormGroup("form_group")
                .withSpan("span")
                .withUserMessage("usser_message"))
                .isNotNull();

        Assertions.assertThat(pathBuilder
                .withFormGroup("form_group")
                .withSpan("span")
                .withUserMessage("usser_message").
                        build())
                .isEqualTo("/div[@class=’form_group’]/span[@id=’usser_message’]");
    }

    @Test
    public void withoutFormGroup(){
        String result = "/div/span[@id=’user-message’ and @class=’form-control’]";        String path = pathSelectorBuilder
                .withSpan("span")
                .withUserMessage("user-message")
                .withFormControl("form-control")
                .build();
        Assert.assertEquals(result, path);
    }

    @Test
    public void withoutSpanUserMessageAndFormControl() {
        String result = "/div[@class=’form-group’]/";
        String path = pathSelectorBuilder
                .withFormGroup("form-group")
                .build();
        Assert.assertEquals(result, path);
    }

    @Test
    public void withoutSpanWithUserMessageAndFormControl(){
        String result = "/div[@class=’form-group’]/";
        String path = pathSelectorBuilder
                .withFormGroup("form-group")
                .withUserMessage("user-message")
                .withFormControl("form-control")
                .build();
        Assert.assertEquals(result, path);
    }

    @Test
    public void withoutSpanAndFormControlWithUserMessage(){
        String result = "/div[@class=’form-group’]/";
        String path = pathSelectorBuilder
                .withFormGroup("form-group")
                .withUserMessage("user-message")
                .build();        Assert.assertEquals(result, path);
    }    @Test
    public void withoutSpanAndUserMessageWithFormControl(){
        String result = "/div[@class=’form-group’]/";
        String path = pathSelectorBuilder
                .withFormGroup("form-group")
                .withFormControl("form-control")
                .build();        Assert.assertEquals(result, path);
    }    @Test
    public void withoutUserMessage() {
        String result = "/div[@class=’form-group’]/span[@class=’form-control’]";
        String path = pathSelectorBuilder
                .withFormGroup("form-group")
                .withSpan("span")
                .withFormControl("form-control")
                .build();
        Assert.assertEquals(result, path);
    }

    @Test
    public void withoutFormControl() {
        String result = "/div[@class=’form-group’]/span[@id=’user-message’]";
        String path = pathSelectorBuilder
                .withFormGroup("form-group")
                .withSpan("span")
                .withUserMessage("user-message")
                .build();
        Assert.assertEquals(result, path);
    }

    @Test
    public void withoutUserMessageAndFormControl(){
        String result = "/div[@class=’form-group’]/span";
        String path = pathSelectorBuilder
                .withFormGroup("form-group")
                .withSpan("span")
                .build();
        Assert.assertEquals(result, path);
    }

    @Test
    public void withNoParametres (){
        String result = "/div/";
        String path = pathSelectorBuilder.build();
        Assert.assertEquals(result, path);
    }

    @Test
    public void withAllParameters() {
        String result = "/div[@class=’form-group’]/span[@id=’user-message’ and @class=’form-control’]";
        String path = pathSelectorBuilder
                .withFormGroup("form-group")
                .withSpan("span")
                .withUserMessage("user-message")
                .withFormControl("form-control")
                .build();
        Assert.assertEquals(result, path);
    }

}