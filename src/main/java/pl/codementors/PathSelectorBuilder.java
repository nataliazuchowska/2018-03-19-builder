package pl.codementors;


import java.util.Objects;

public class PathSelectorBuilder {
    private String form_group;
    private String span;
    private String user_message;
    private String form_control;


    public PathSelectorBuilder withFormGroup(String form_group) {
        this.form_group = form_group;
        return this;
    }

    public PathSelectorBuilder withSpan (String span){
        this.span = span;
        return this;
    }

    public PathSelectorBuilder withUserMessage (String user_message){
        this.user_message = user_message;
        return this;
    }

    public PathSelectorBuilder withFormControl (String form_control){
        this.form_control =form_control;
        return this;
    }

    String build() {

        checkParameterFormGroup();
        checkParameterSpan();
        checkParameterUserMessage();
        checkParameterFormControl();

        return "/div" + form_group + "/" + span + user_message + form_control;
    }

    public void checkParameterFormGroup (){
        if (form_group != null) {
            this.form_group = "[@class=’" + form_group + "’]";
        }
        if (form_group == null || Objects.equals(form_group, "")) {
            form_group = "";
        }
    }

    public void  checkParameterSpan (){
        if (span == null|| Objects.equals(span, "") ){
            span = "";
        }
    }

    public void  checkParameterUserMessage (){
        if (span != null && form_control == null && user_message != null) {
            this.user_message = "[@id=’" + user_message + "’]";
            }
            if (span != null && form_control != null && user_message != null) {
            this.user_message = "[@id=’" + user_message + "’";
            }
            if (span == null || Objects.equals(span, "")
                    || user_message == null
                    || Objects.equals(user_message, "")) { user_message = ""; }
    }

    public void checkParameterFormControl (){
        if (span != null && (user_message == null || Objects.equals(user_message, ""))
                && form_control != null) {
            this.form_control = "[@class=’" + form_control + "’]";

        } else if (span != null && (user_message != null
                || !Objects.equals(user_message, ""))
                && form_control != null) {
            this.form_control = " and @class=’" + form_control + "’]";

        }if (span == null || Objects.equals(span, "") ||
                form_control == null || Objects.equals(form_control, "")) {
            form_control = "";
        }
    }
}
